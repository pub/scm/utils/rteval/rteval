# SPDX-License-Identifier: GPL-2.0-or-later
#
#   Copyright 2015  Clark Williams <williams@redhat.com>
#   Copyright 2012 - 2013   David Sommerseth <davids@redhat.com>
#   Copyright 2024  John Kacur <jkacurWredhat.com>
#

RTEVAL_VERSION = '3.9'
